package main

import (
	_ "gitlab.com/F3r/design-patterns/src/app/routers"
	"github.com/astaxie/beego"
)

func main() {
	beego.SetViewsPath("src/app/views")
	beego.Run()
}
