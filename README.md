# Design Patterns

*  Strategy


## Prerequisite Tools

* Git
* Go >= 1.9

## Install

```bash
## download project
go get gitlab.com/F3r/design-patterns

## install dependency beego (web framework)
go get github.com/astaxie/beego

```

## Run

```bash
cd ${GOPATH:-$HOME/go}/src/gitlab.com/F3r/design-patterns
go run main.go
## open http://localhost:8080

```