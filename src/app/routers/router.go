package routers

import (
	"gitlab.com/F3r/design-patterns/src/app/controllers"
	"github.com/astaxie/beego"
)

func init() {
	beego.Router("/", &controllers.StrategyController{})
    beego.Router("/:sortAlgorithm/:quantity:int", &controllers.StrategyController{})
}
