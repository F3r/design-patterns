<!DOCTYPE html>

<html>
<head>
  <title>Strategy</title>
   <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
   <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
   <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
   <script language=”JavaScript”>
        $('.dropdown-menu a').click(function(){
            $('#selected').text($(this).text());
        });
   </script>
</head>

<body>
    <div class="container">
        <h1>Strategy</h1>
        <p>Sample implementation generate and sort random numbers.</p>

        <div class="form-group row">
          <div class="col-xs-3">
            <label>Quantity Numbers</label>
            <input class="form-control" id="quantity" type="number" value="0">
          </div>
          <div class="col-xs-3">
            <label>Sort Algorithm</label>
            <select class="form-control" id="sortAlgorithm">
                <option>Bubble</option>
                <option>Merge</option>
                <option>Quick</option>
              </select>
          </div>
        </div>
        <div class="form-group row">
            <div class="col-xs-3">
                <button type="button" class="btn btn-primary"
                onclick="location.href='/' + document.getElementById('sortAlgorithm').value + '/' + document.getElementById('quantity').value" >Generate Numbers</button>
            </div>
        </div>

         <div class="form-row" id="list" style="{{.IsHide}}" >
            <div class="col-md-4 mb-3">
            <label for="validationCustom01">Numbers</label>
                <ul class="list-group">
                        {{.ContentList}}
                </ul>
            </div>
         </div>

    </div>
    <script src="/static/js/reload.min.js"></script>
</body>
</html>
