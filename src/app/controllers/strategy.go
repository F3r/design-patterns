package controllers

import (
	"github.com/astaxie/beego"
	"strconv"
	"gitlab.com/F3r/design-patterns/src/domain/strategy"
	"gitlab.com/F3r/design-patterns/src/domain/strategy/sorting"
	"bytes"
	"fmt"
	"html/template"
)

type StrategyController struct {
	beego.Controller
}

func (c *StrategyController) Get() {
	sortAlgorithm := c.Ctx.Input.Param(":sortAlgorithm")
	quantity, _ := strconv.Atoi(c.Ctx.Input.Param(":quantity"))

	display := "display:none"

	if sortAlgorithm != "" && quantity > 0 {

		numberGenerator := strategy.NewNumbersGenerator()


		switch sortAlgorithm {
		case "Bubble":
			numberGenerator.SetSortAlgorithm(sorting.NewBubbleSort())
		case "Merge":
			numberGenerator.SetSortAlgorithm(sorting.NewMergeSort())
		case "Quick":
			numberGenerator.SetSortAlgorithm(sorting.NewQuickSort())
		}

		listNumbers := numberGenerator.GenerateAndSort(quantity)

		var buffer bytes.Buffer
		for _, number := range listNumbers {

			stringLine :=  fmt.Sprintf("<li class='list-group-item'>%v</li>",number)
			buffer.WriteString(stringLine)
		}

		display = ""
		c.Data["ContentList"] = template.HTML(buffer.String())
	}

	c.Data["IsHide"] =  display
	c.TplName = "strategy.tpl"

}
