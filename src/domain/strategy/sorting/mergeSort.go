package sorting

type mergeSort struct {

}

func NewMergeSort() AlgorithmInterface {
	return  mergeSort{}
}

func (aa mergeSort) Sort(a []int) {
	merge(a)
}

func merge(a []int) {
	var s = make([]int, len(a)/2+1)
	if len(a) < 2 {
		return
	}
	mid := len(a) / 2
	merge(a[:mid])
	merge(a[mid:])
	if a[mid-1] <= a[mid] {
		return
	}
	// merge step, with the copy-half optimization
	copy(s, a[:mid])
	l, r := 0, mid
	for i := 0; ; i++ {
		if s[l] <= a[r] {
			a[i] = s[l]
			l++
			if l == mid {
				break
			}
		} else {
			a[i] = a[r]
			r++
			if r == len(a) {
				copy(a[i+1:], s[l:mid])
				break
			}
		}
	}
}