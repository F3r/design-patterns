package sorting

type AlgorithmInterface interface {
	Sort(array []int)
}