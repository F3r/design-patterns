package sorting

type bubbleSort struct {

}

func NewBubbleSort() AlgorithmInterface {
	return bubbleSort{}
}

func (bubble bubbleSort) Sort(a []int) {
	for itemCount := len(a) - 1; ; itemCount-- {
		hasChanged := false
		for index := 0; index < itemCount; index++ {
			if a[index] > a[index+1] {
				a[index], a[index+1] = a[index+1], a[index]
				hasChanged = true
			}
		}
		if hasChanged == false {
			break
		}
	}
}