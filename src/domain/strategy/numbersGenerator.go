package strategy

import (
	"math/rand"
	"time"

	"gitlab.com/F3r/design-patterns/src/domain/strategy/sorting"
)

type numbersGenerator struct {
	max              int
	min              int
	sortingAlgorithm sorting.AlgorithmInterface
}

func NewNumbersGenerator() numbersGenerator {
	return numbersGenerator{
		max:              1000,
		min:              0,
		sortingAlgorithm: sorting.NewBubbleSort(),
	}
}

func (ng numbersGenerator) SetSortAlgorithm(sortAlgorithm sorting.AlgorithmInterface)  {
	ng.sortingAlgorithm = sortAlgorithm
}

func (ng numbersGenerator) GenerateAndSort(quantity int) []int {
	listNumbers := generateRandomNumbers(quantity, ng.min, ng.max)
	sort(listNumbers, ng.sortingAlgorithm)
	return listNumbers
}

func generateRandomNumbers(quantity int, min int, max int) []int {
	listNumbers := make([]int, 0)
	for i := 1; i <= quantity; i++ {
		listNumbers = append(listNumbers, randInt(min, max))
	}
	return listNumbers
}

func randInt(min int, max int) int {
	rand.Seed(time.Now().UTC().UnixNano())
	return min + rand.Intn(max-min)
}

func sort(a []int, sortingAlgorithm sorting.AlgorithmInterface) {
	if sortingAlgorithm == nil {
		return
	}
	sortingAlgorithm.Sort(a)
}
