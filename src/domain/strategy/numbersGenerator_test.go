package strategy

import (
	"testing"
	"gitlab.com/F3r/design-patterns/src/domain/strategy/sorting"
)

func TestGenerateNumbers(t *testing.T) {

	const QuantityNumbers = 5

	generateNumber :=  NewNumbersGenerator()
	arrayNumbers := generateNumber.GenerateAndSort(QuantityNumbers)

	if len(arrayNumbers) != QuantityNumbers {
		t.Error("expected",QuantityNumbers,"got",len(arrayNumbers))
	}

}

func TestBubbleSort(t *testing.T) {

	const QuantityNumbers = 5

	generateNumber :=  NewNumbersGenerator()
	generateNumber.sortingAlgorithm = sorting.NewBubbleSort()
	sortArray := generateNumber.GenerateAndSort(QuantityNumbers)

	if len(sortArray) != QuantityNumbers {
		t.Error("expected",QuantityNumbers,"got",len(sortArray))
	}
	if !verifySort(sortArray) {
		t.Error("Bubble sort don't work ",sortArray)
	}

}

func TestQuickSort(t *testing.T) {

	const QuantityNumbers = 5

	generateNumber :=  NewNumbersGenerator()
	generateNumber.sortingAlgorithm = sorting.NewQuickSort()
	sortArray := generateNumber.GenerateAndSort(QuantityNumbers)

	if len(sortArray) != QuantityNumbers {
		t.Error("expected",QuantityNumbers,"got",len(sortArray))
	}
	if !verifySort(sortArray) {
		t.Error("Quick sort don't work ",sortArray)
	}

}

func TestMergeSort(t *testing.T) {

	const QuantityNumbers = 5

	generateNumber :=  NewNumbersGenerator()
	generateNumber.sortingAlgorithm = sorting.NewMergeSort()
	sortArray := generateNumber.GenerateAndSort(QuantityNumbers)

	if len(sortArray) != QuantityNumbers {
		t.Error("expected",QuantityNumbers,"got",len(sortArray))
	}
	if !verifySort(sortArray) {
		t.Error("Merge sort don't work ",sortArray)
	}

}

func verifySort(a []int) bool {
	if len(a) < 2 {
		return false
	}
	for i := 1; i < len(a); i++ {
		if a[i] < a[i-1] {
			return false
		}
	}
	return true
}

